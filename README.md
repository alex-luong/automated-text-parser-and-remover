# bot-text

**Purpose**
A discord bot to move any messages created by bots into the bots channel and delete the original message 
from the current channel. 

This is meant to clean up normal chat channels and ensure that messages aren't ignored due to a torrent of 
bot commands. Hopefully, without the bot commands interrupting discussions, servers and people can continue to converse and connect

**Installation instructions**
This bot is meant to be hosted on Heroku, the Procfile would automatically run the server, simply add the bot into your channel.
Furthermore, the client id and token is based on Heroku environmental values, change those to your bot client id and token if you
wish to run it without heroku. 

% node index.js
Will run the bot through your terminal.